import requests
import slackap
import os
from flask import Flask
from flask import request
from multiprocessing import Process

cwd=os.getcwd()+'\\files'#директория для работы с файлами
SLACK_WEBHOOCK_URL="https://hooks.slack.com/services/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"#наш вебхук
api='xoxb-1167390180566-1216054484020-x88p2hbdbXOJ7XAsAj5LcmXv'#токн бота
api_methods=slackap.slackApi(api,cwd)

app = Flask(__name__)

def process(url):
    data={'token':api,'channel':'#general','text':'Обработка выполнилась'}
    requests.post('https://slack.com/api/chat.postMessage',data=data)
    name=api_methods.download_file(url)
    api_methods.load_file(name)


@app.route('/api/v1/event_sub/',methods=['POST'])
def verif_event():
   
    resp_verif=request.json   
    print(resp_verif)
    
    if 'files' in resp_verif['event']:
        files=resp_verif['event']['files'][0]['url_private_download']
        api_methods.post_message('#general','Обработка файла')
        global p
        p = Process(target=process, args=(files,))
        p.start()
        return 'ok'
    else:
        text=resp_verif['event']['blocks'][0]['elements'][0]['elements'][1]['text']
        api_methods.post_message('#general',text)
        return 'ok'  
 

if __name__ == '__main__':
    app.run(host='localhost', port=80, threaded=True)

